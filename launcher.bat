@echo off
goto check_Permissions
:check_Permissions
    echo Administrative permissions required. Detecting permissions...
    
    net session >nul 2>&1
    if %errorLevel% == 0 (
        echo Success: Administrative permissions confirmed.
    ) else (
        echo You must be running as user administrator to run this program.
        exit /b 5
    )
    
goto launcher

:launcher
ps sample-multimenu.ps1